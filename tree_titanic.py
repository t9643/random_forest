import pandas as pd
from sklearn import tree
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

titanic_data = pd.read_csv('train.csv')

pd.set_option('display.max_columns', None)
pd.options.display.expand_frame_repr = False

X = titanic_data.drop(["PassengerId", "Survived", "Name", "Ticket", "Cabin"], axis = 1)
Y = titanic_data.Survived
X = pd.get_dummies(X)
#Заполняем пропуски в Age Средним (медиана) значением
X = X.fillna({'Age':X.Age.median()})

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size = 0.33, random_state = 42)

clf = tree.DecisionTreeClassifier(criterion = 'entropy', max_depth = 3,
                                  min_samples_split = 100, min_samples_leaf = 10)

clf.fit(X_train, Y_train)

fig, axe = plt.subplots(figsize=(10, 5))
tree.plot_tree(clf, ax = axe, fontsize=10, filled = True,
               feature_names = list(X), class_names = ['Died', 'Survived'])

print(plt.show())


